﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EuroMilhoes
{
    class Chave
    {
        private List<byte> nums;
        private List<byte> estrelas;
        static private Random ran;
        static private string saveFile = @"favoritos.txt";

        #region construtores
        public Chave()
        {
            nums = new List<byte>();
            estrelas = new List<byte>();
            for (byte i = 0; i < 5; i++) {
                this.nums.Add(0);
            }
            for (byte i = 0; i < 2; i++)
            {
                this.estrelas.Add(0);
            }
        }

        public Chave(string[] n, string[] e) {
            setNums(n);
            setEst(e);
            this.sortKey();
        }

        public Chave(Chave ch) {
            this.nums = ch.nums;
            this.estrelas = ch.estrelas;
        }
        #endregion
        
        #region seletores
        public bool setNums(string[] n)
        {
            try
            {
                this.nums.Clear();
                byte bNum;
                foreach (string num in n)
                {
                    if (num != "")
                    {
                        bNum = byte.Parse(num);
                        if (bNum > 0 && bNum < 51)
                        {
                            this.nums.Add(bNum);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Formato invalido.");
                return false;
            }
            return true;
        }

        public List<byte> getNums()
        {
            return this.nums;
        }

        public bool setEst(string[] est)
        {
            try
            {
                this.estrelas.Clear();
                byte bNum;
                foreach (string num in est)
                {
                    if (num != "")
                    {
                        bNum = byte.Parse(num);
                        if (bNum > 0 && bNum < 13)
                        {
                            this.estrelas.Add(bNum);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Formato invalido.");
                return false;
            }
            return true;
        }

        public List<byte> getEstrelas()
        {
            return this.estrelas;
        }

        public override string ToString()
        {
            string numConc = "";
            string estConc = "";
            foreach (byte num in this.nums)
            {
                numConc = numConc + num + " ";
            }
            foreach (byte num in this.estrelas)
            {
                estConc = estConc + num + " ";
            }
            return string.Format("{0}- {1}", numConc, estConc);
        }
        #endregion
     
        private void sortKey()
        {
            this.nums.Sort();
            this.estrelas.Sort();
        }
        
        public bool keyValida()
        {
            bool flag = this.estrelas.Count < 2 || this.estrelas.Count > 3 || this.nums.Count < 5 || this.nums.Count >10 ? false : this.nums.Any(x => x == 0) ? false : this.estrelas.Any(y => y == 0) ? false : !this.nums.Any()? false : true;
            if(this.estrelas.Distinct().Count() != this.estrelas.Count())
            {
                return false;
            }
            if (this.nums.Distinct().Count() != this.nums.Count())
            {
                return false;
            }
            return flag;
        }

        public void limparChave()
        {
            this.nums.Clear();
            this.estrelas.Clear();
        }

        public void randomizeKey()
        {
            ran = new Random();
            System.Threading.Thread.Sleep(1000);
            byte aleatorio = 0;
            this.limparChave();

            for (byte i = 0; i < 5; i++)
            {
                bool repetido = false;
                do
                {
                    aleatorio = (byte)ran.Next(1, 50);
                    if (!this.nums.Any(x => x == aleatorio))
                    {
                        repetido = true;
                    }
                } while (!repetido);
                this.nums.Add(aleatorio);
            }
            for (byte i = 0; i < 2; i++)
            {
                bool repetido = false;
                do
                {
                    aleatorio = (byte)ran.Next(1, 12);
                    if (!this.estrelas.Any(x => x == aleatorio))
                    {
                        repetido = true;
                    }
                } while (!repetido);
                this.estrelas.Add(aleatorio);
            }
            this.sortKey();
        }
        
        public string checkPremio()
        {
            Chave premio = new Chave();
            premio.randomizeKey();
            byte countnum = 0;
            byte countest = 0;


            string numConc = "";
            string estConc = "";
            foreach (byte num in premio.nums)
            {
                if (this.nums.Any(x => x == num)) { 
                    numConc = numConc + "*" + num+ "* ";
                    countnum++;
                }
                else
                {
                    numConc = numConc + num + " ";
                }
            }
            foreach (byte num in premio.estrelas)
            {
                if (this.estrelas.Any(x => x == num))
                {
                    estConc = estConc + "*" + num + "* ";
                    countest++;
                }
                else
                {
                    estConc = estConc + num + " ";
                }
            }
            string chavePremio = string.Format("{0}- {1}", numConc, estConc);
            
            string mensagem = countnum == 5 && countest == 2 ? "1º Prémio!" : countnum  == 5 && countest == 1 ? "2º Prémio!" : countnum == 5 && countest == 0 ? "3º Prémio!" : countnum == 4 && countest == 2 ? "4º Prémio!" : "Vai ao site, 13 premios...";
            return string.Format("\nA sua chave    | {0}\nChave premiada | {1}\n\nAcertou {2} numeros e {3} estrelas.\n{4}", ToString(), chavePremio, countnum, countest, mensagem);

        }

        public bool stringToKey(string linha)
        {
            try
            {
                this.limparChave();

                string[] full = linha.Split('-');
                string[] nums = full[0].Split(' ');
                string[] estrelas = full[1].Split(' ');
                this.setNums(nums);
                this.setEst(estrelas);
                if(!this.keyValida()){return false;}
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        #region static
        public static List<string> getKeys()
        {
            StreamReader file = new StreamReader(saveFile);
            List<string> favoritos = new List<string>();

            try
            {
                while (!file.EndOfStream) { 
                    favoritos.Add(file.ReadLine());
                }
            }
            catch(Exception)
            {
                Console.WriteLine("Ocorreu um erro.");
            }
            finally
            {
                file.Close();
            }
            return favoritos;
        }

        public static void saveKeys(Chave ch)
        {
            bool check = File.Exists(saveFile) ? true : false;
            StreamWriter file = new StreamWriter(saveFile, check);
            try
            {
                file.WriteLine(ch.ToString());
            }
            catch (Exception)
            {
                Console.WriteLine("Ocorreu um erro.");
            }
            finally
            {
                file.Close();
            }
        }

        public static bool ficheiroExiste()
        {
            bool check = File.Exists(saveFile) ? true : false;
            return check;
        }

        public static void deleteFavs()
        {
            Console.Clear();
            Console.WriteLine("A verificar ficheiro...");
            System.Threading.Thread.Sleep(2000);
            bool check = File.Exists(saveFile) ? true : false;

            try
            {
                File.Delete(saveFile);
                Console.WriteLine("Favoritos apagados.");
                System.Threading.Thread.Sleep(2000);
            }
            catch(Exception)
            {
                Console.WriteLine("Ocorreu um erro.");
            }
        }
        #endregion

    }
}
