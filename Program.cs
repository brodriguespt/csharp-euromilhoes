﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuroMilhoes
{
    class Program
    {
        enum mainM { InserirChave=1, Apostar=2, ApagarFavoritos=3, Sair=4};
        enum betM { Usar=1, Favoritos=2, Sair=3};
        
        static byte checkByte()
        {
            byte numero = 0;
            bool flag = true;
            do
            {
                try
                {
                    numero = byte.Parse(Console.ReadLine());
                    flag = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Valor incorrecto, digite novamente:");
                    flag = false;
                }
            } while (!flag);

            return numero;
        }

        static void fechar()
        {
            Console.Clear();
            Console.WriteLine("Obrigado!");
            System.Threading.Thread.Sleep(2000);
            Environment.Exit(0);
        }
        
        static byte mainMenu()
        {
            byte count = 0;
            Console.Clear();
            Console.WriteLine("Qual a opção pretendida?");
            foreach (mainM val in Enum.GetValues(typeof(mainM)))
            {
                Console.WriteLine(++count + " - " + val);
            }
            return checkByte();
        }

        static void preencherChave( Chave userKey)
        {
            Console.Clear();
            bool flag = true;
            Console.WriteLine("Insira a chave no seguinte formato: 0 0 0 0 - 0 0\nPode usar no minimo 5 numeros e 2 chaves e no maximo 10 numeros e 3 chaves.");
            do {
                if (userKey.stringToKey(Console.ReadLine()))
                {
                    Console.WriteLine("Chave guardada: {0}", userKey.ToString());
                    flag = true;
                }
                else
                {
                    Console.WriteLine("Chave inválida, tente outra vez.");
                    flag = false;
                }
            } while (!flag);
            System.Threading.Thread.Sleep(2000);
        }
        
        static void betMenu(Chave userKey)
        {
            bool favs = Chave.ficheiroExiste() ? true: false;
            bool randomKey = userKey.keyValida() ? true : false;
            byte count = 0;
            byte escolha = 0;
            List<string> favoritos = new List<string>();

            Console.Clear();
            string mensagem = randomKey ? string.Format("{0} - Sua chave | {1}", count, userKey.ToString()): string.Format("{0} - Não tem uma chave. Se escolher esta opção vai jogar com uma chave aleatoria.", count);

            Console.WriteLine("Para apostar escolha o numero da chave que pretende jogar.");
            Console.WriteLine(mensagem);

            if (favs)
            {
                favoritos = Chave.getKeys();
                foreach (string key in favoritos)
                {
                    if(key != "") { 
                        Console.WriteLine("{0} - Chave fav | {1}", ++count, key);
                    }
                }
            }
            if (count == 0) { Console.WriteLine("Sem favoritos."); }

            do {
                Console.Write("\nChave:");
                escolha = checkByte();
            } while (escolha < 0 || escolha > count);
            if(escolha == 0)
            {
                if(!randomKey)
                {
                    userKey.randomizeKey();
                }
                Console.WriteLine(userKey.checkPremio());
                do
                {
                    Console.WriteLine("\nQuer guardar a chave jogada nos favoritos?");
                    Console.WriteLine("0 | Sim     1 | Não");
                    escolha = checkByte();
                } while (escolha < 0 || escolha > 1);
                if (escolha == 0) { Chave.saveKeys(userKey); }
            }
            else
            {
                userKey.stringToKey(favoritos[escolha-1]);
                Console.WriteLine(userKey.checkPremio());
            }

            userKey.limparChave();
            Console.WriteLine("\nPrima qualquer tecla para continuar.");
            Console.ReadKey();

        }

        static void Main(string[] args)
        {
            Chave userKey = new Chave();
            byte opcao;
            do
            {
                opcao = mainMenu();
                switch (opcao)
                {
                    case 1:
                        preencherChave(userKey);
                        break;
                    case 2:
                        betMenu(userKey);
                        break;
                    case 3:
                        Chave.deleteFavs();
                        break;
                    case 4:
                        break;
                    default:
                        Console.WriteLine("Opção inválida!");
                        break;
                }
            } while (opcao != (byte)mainM.Sair);
            System.Threading.Thread.Sleep(1000);
        }
    }
}
